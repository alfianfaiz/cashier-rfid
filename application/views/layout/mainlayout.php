<?php 
	$asset = base_url().'assets/';
 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/costom.css">

	<!-- <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">	 -->
	<title>Cashier</title>
</head>
<body>

<div class="container">
<!-- header -->
	<div  class="header">
			<center><font style="font-family:Lato;font-size:5em;"> Cashier</font><small><b>Admin</b>version</small></center>

	</div>
	<hr><br>
	<!-- <div class="navbar normalbox noborderradius">
		<div class="container-fluid ">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url() ?>home/"> <span class="glyphicon glyphicon-home"></span> Beranda</a></li>
				<li><a href="<?php echo base_url() ?>home/panduan"><span class="glyphicon glyphicon-th-large"></span> Panduan</a></li>
				<li><a href="<?php echo base_url() ?>home/kontak"><span class="glyphicon glyphicon-envelope"></span> Kontak Kami</a></li>
				<?php if (!$this->session->userdata('IS_LOGGED_IN')): ?>
					<li><a href="<?php echo base_url() ?>home/pendaftaran"><span class="glyphicon glyphicon-edit"></span> Pendaftaran</a></li>
					<li><a href="<?php echo base_url() ?>auth/out">Logout</a></li>
				<?php endif ?>
				
			</ul>
		</div>
	</div> -->
	
	<div class="row">
		<div class="col-md-3 " id="sidebar" >
			<ul class="nav nav-pills nav-stacked normalbox leftnavbox" data-spy="affix" data-offset-top="155">
					<li><a href="<?php echo base_url() ?>administration/penjualan"> <span class="glyphicon glyphicon-pencil"></span> Penjualan Hari ini</a></li>
					<li><a href="<?php echo base_url() ?>administration/uploadbarang"> <span class="glyphicon glyphicon-unchecked"></span> Upload Barang</a></li>
					<li><a href="<?php echo base_url() ?>administration/databarang"> <span class="glyphicon glyphicon-user"></span> Data Barang</a></li>
					<li><a href="<?php echo base_url() ?>administration/datapetugas"> <span class="glyphicon glyphicon-list-alt"></span> Data Petugas Outlet</a></li>
					<li><a href="<?php echo base_url() ?>administration/kritiksaran"> <span class="glyphicon glyphicon-duplicate"></span> Kritik dan Saran</a></li>
					<li><a href="<?php echo base_url() ?>auth/out"> <span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>	
		</div>
		<div class="col-md-9 normalbox normalpadding" style="min-height:500px;margin-bottom:100px">
			<?php $this->load->view('page/'.$page) ?>
		</div>
	</div>
	<div class="footerku">
		
	</div>
</div>
<script type="text/javascript" src="<?php echo $asset ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $asset ?>js/bootstrap.js"></script>
</body>
</html>

