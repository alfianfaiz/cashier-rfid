<div class="container-fluid">
	<h3>Form Upload Barang</h3>
	<dd>Inputkan data barang yang akan diupload lalu klik scan kode barang untuk mencatatkan kode rfid barang</dd>
	<br>
	<form class="form-horizontal col-md-9" method="post" action="<?php echo base_url() ?>administrasi/simpanbarang/">
		<div class="form-group">
		    <label class="col-sm-3 control-label">Nama Barang</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang">
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Jenis Barang</label>
		    <div class="col-sm-9">
		      <select name="jenis_barang" class="form-control">
		      	<option value="1">Shirts</option>
		      	<option value="1">Pants</option>
		      	<option value="1">Dresses</option>
		      	<option value="1">Jackets</option>
		      </select>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Bahan Barang</label>
		    <div class="col-sm-9">
		      <select name="bahan_barang" class="form-control">
		      	<option value="1">Shirts</option>
		      	<option value="1">Pants</option>
		      	<option value="1">Dresses</option>
		      	<option value="1">Jackets</option>
		      </select>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Warna Barang</label>
		    <div class="col-sm-9">
		      <select name="warna_barang" class="form-control">
		      	<option value="1">Hijau</option>
		      	<option value="1">Merah</option>
		      	<option value="1">Biru</option>
		      	<option value="1">Coklat</option>
		      </select>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Harga</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="harga_barang" placeholder="Harga">
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Stock</label>
		    <div class="col-sm-3">
		      <input type="text" class="form-control" name="stock_barang" placeholder="Stock">
		    </div>
		    <div class=" col-sm-6">
		    	<button type="button" class="btn btn-default form-control" data-toggle="modal" data-target="#myModal">Scan Kode Barang</button>
		    </div>
		</div>
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-3">
				<button class="btn btn-success form-control" type="submit">Simpan</button>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <center><h4>Scan Kode Barang</h4></center>
		      </div>
		      <div class="modal-body">
		        <div class="container-fluid">
		        	<div class="form-group">
					    <label class="col-sm-12">Barang 1</label>
					    <div class="col-sm-4">
					      <input type="text" class="form-control" name="kode_barang[1]" placeholder="x-x-x">
					    </div>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" name="ukuran_barang[1]" placeholder="Ukuran Barang">
					    </div>
					    <div class=" col-sm-5 btn btn-default">
					    	<input type="file" name="gambar_barang[1]">
					    </div>
					</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>
</div>
