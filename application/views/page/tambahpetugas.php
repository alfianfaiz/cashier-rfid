<div class="container-fluid">
	<h3>Form Tambah Petugas</h3>
	<dd>Inputkan data petugas lalu klik simpan untuk mendaftar</dd>
	<br>
	<form class="form-horizontal col-md-9" method="post" action="<?php echo base_url() ?>administration/simpanpetugas/"  enctype="multipart/form-data">
		<div class="form-group">
		    <label class="col-sm-3 control-label">Nama Lengkap Petugas</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="nama_petugas" placeholder="Nama Petugas" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Nomor Identitas</label>
		    <div class="col-sm-9">
		      <input type="number" class="form-control" name="nomor_identitas_petugas" placeholder="Nomor Identitas" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Username</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="username_petugas" placeholder="Username" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Password</label>
		    <div class="col-sm-9">
		      <input type="password" class="form-control" name="password_petugas" placeholder="Password" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Email</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="email_petugas" placeholder="Email" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Alamat</label>
		    <div class="col-sm-9">
		      <textarea name="alamat_petugas" class="form-control" required>Alamat</textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3 control-label">Foto</label>
		    <div class="col-sm-9">
		      <input type="file" class="form-control" name="userfile" placeholder="Foto" required>
		    </div>
		</div>
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-3">
				<button class="btn btn-success form-control" type="submit">Simpan</button>
			</div>
		</div>
	</form>
</div>
