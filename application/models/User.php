<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {

	public function getall()
	{
		$this->db->where('level', '2');
		$this->db->where('status', 1);
		$data = $this->db->get('user')->result();
		return $data;
	}	

	public function getDataUser($id_user)
	{
		$this->db->where('id_user', $id_user);
		return $this->db->get('User')->result();
	}

}

/* End of file User.php */
/* Location: ./application/models/User.php */