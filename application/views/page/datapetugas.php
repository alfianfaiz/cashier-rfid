<div class="container-fluid">
	<h3>Data Petugas</h3>
	<div class="col-sm-3 nopadding">
		<a href="<?php echo base_url() ?>administration/tambahpetugas" class="btn btn-default">Tambah Petugas</a>
		<br><br>
	</div>

	<table class="table table-striped">
		<tr>
			<td>Username</td>
			<td>Password</td>
			<td>Nama</td>
			<td>TTL</td>
			<td>Alamat</td>
			<td>ID KTP</td>
			<td>Foto</td>
			<td>Aksi</td>
		</tr>
		<?php foreach ($datapetugas as $key): ?>
			<tr>
				<td><?php echo $key->username ?></td>
				<td><?php echo $key->password ?></td>
				<td><?php echo $key->nama_lengkap ?></td>
				<td><?php// echo $key->ttl ?></td>
				<td><?php echo $key->alamat ?></td>
				<td><?php echo $key->no_identitas ?> KTP</td>
				<td><?php echo $key->foto ?></td>
				<td>
					<div class="btn-group" role="group" >
					  <a href="<?php echo base_url() ?>administration/editpetugas/<?php echo $key->id_user ?>" class="btn btn-default btn-sm">Edit</a>
					  <a href="<?php echo base_url() ?>administration/hapuspetugas/<?php echo $key->id_user ?>" class="btn btn-default btn-sm">Hapus</a>
					</div>
				</td>
			</tr>
		<?php endforeach ?>
	</table>
</div>
