<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidan extends CI_Model {

	public function getAllBidan()	
	{
		$this->db->join('puskesmas', 'bidan.puskesmas_id = puskesmas.id', 'left');
		$this->db->select('bidan.*,puskesmas.kelurahan');
		$data = $this->db->get('bidan')->result();
		return $data;
	}

	public function getAllBidanByPuskesmas($puskesmas_id = null,$keyword = null)	
	{
		if($keyword != null)
		{
			$this->db->like('kode_pendaftaran',$keyword);
		}
		if($puskesmas_id != null)
			$this->db->where('puskesmas_id',$puskesmas_id);
		$this->db->join('puskesmas', 'bidan.puskesmas_id = puskesmas.id', 'left');
		$this->db->select('bidan.*,puskesmas.kelurahan');
		$this->db->order_by('nilai_akhir', 'desc');
		$data = $this->db->get('bidan')->result();
		return $data;
	}

	public function getAllBidanDiPuskesmas($id_puskesmas)	
	{
		$this->db->where('puskesmas.id', $id_puskesmas);
		$this->db->join('puskesmas', 'bidan.puskesmas_id = puskesmas.id', 'left');
		$data = $this->db->get('bidan')->result();
		return $data;
	}

	public function getDataBidan($bidan_id)
	{
		$this->db->where('bidan.id', $bidan_id);
		$data = $this->db->get('bidan')->result();
		return $data;
	}

	public function getKelasKriteriaBidan($bidan_id,$kriteria)
	{
		$this->db->where('bidan.id', $bidan_id);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->join('bidan_kriteria_nilai', 'bidan.id = bidan_kriteria_nilai.bidan_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = bidan_kriteria_nilai.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$data = $this->db->get('bidan')->result();
		$result = 0;
		foreach ($data as $key) {
			$result = $key->nama_kelas;
		}
		return $result;
	}

	public function getIDKelasKriteriaBidan($bidan_id,$kriteria)
	{
		$this->db->where('bidan.id', $bidan_id);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->join('bidan_kriteria_nilai', 'bidan.id = bidan_kriteria_nilai.bidan_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = bidan_kriteria_nilai.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('kelas_kriteria.id as ID');
		$data = $this->db->get('bidan')->result();
		$result = 0;
		foreach ($data as $key) {
			$result = $key->ID;
		}
		return $result;
	}

	public function getSkorKriteria($bidan_id,$kriteria)
	{
		$this->db->where('bidan.id', $bidan_id);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->join('bidan_kriteria_nilai', 'bidan.id = bidan_kriteria_nilai.bidan_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = bidan_kriteria_nilai.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->select('bidan_kriteria_nilai.skor as skor');
		$data = $this->db->get('bidan')->result();
		$result = 0;
		foreach ($data as $key) {
			$result = $key->skor;
		}
		return $result;
	}

	public function deleteKriteriaNilaiByKriteria($kode_pendaftaran,$kriteria)
	{
		$this->db->join('bidan_kriteria_nilai', 'bidan.id = bidan_kriteria_nilai.bidan_id', 'left');
		$this->db->join('kelas_kriteria', 'kelas_kriteria.id = bidan_kriteria_nilai.kelas_kriteria_id', 'left');
		$this->db->join('kriteria', 'kelas_kriteria.kriteria_id = kriteria.id', 'left');
		$this->db->where('bidan.kode_pendaftaran', $kode_pendaftaran);
		$this->db->where('kriteria.kriteria', $kriteria);
		$this->db->select('bidan_kriteria_nilai.id');
		$data = $this->db->get('bidan')->result();
		return $data;
		// $this->db->delete('bidan_kriteria_nilai');
	}

	public function updateNilaiAkhir($NA,$bidan_id)
	{
		$this->db->set('nilai_akhir',$NA);
		$this->db->where('id', $bidan_id);
		$this->db->update('bidan');
	}

}

/* End of file Bidan.php */
/* Location: ./application/models/Bidan.php */