<?php 
	$dataJenis = $model_tool->getJenisList();
 ?>
<div class="container-fluid">
	<form method="get" >
		<h3>Data Barang</h3>
		<div class="col-sm-3 nopadding">
			<select id="keyword" class="form-control" name="keyword">
				<?php foreach ($dataJenis as $key): ?>
					<option value="<?php echo underscore($key->nama_jenis) ?>"><?php echo $key->nama_jenis ?></option>
				<?php endforeach ?>
			</select>
			<br>
		</div>
		<div class="col-sm-3 ">
			<button type="submit" class="btn btn-default">Cari</button>
			<br>
		</div>
	</form>
	
	<table class="table table-striped">
		<tr>
			<td colspan="8">
				<span class="alert-danger">
					<?php echo $this->session->flashdata('error'); ?>
				</span>
				<span class="alert-success">
					<?php echo $this->session->flashdata('success'); ?>
				</span>
			</td>
		</tr>
		<tr>
			<td>ID</td>
			<td>Nama Barang</td>
			<td>Jenis</td>
			<td>Warna</td>
			<td>Harga</td>
			<td>Gambar</td>
			<td>Pemilik</td>
			<td>Aksi</td>
		</tr>
		<?php foreach ($databarang as $key): ?>
			<tr>
				<td><?php echo $key->nomor_unit ?></td>
				<td><?php echo $key->nama_barang ?></td>
				<td><?php echo $key->nama_jenis ?></td>
				<td><?php echo $key->nama_warna ?></td>
				<td><?php echo $key->harga ?></td>
				<td><?php echo $key->foto_barang ?></td>
				<td>
					<?php if ($key->nama_lengkap != null): ?>
						<?php echo $key->nama_lengkap ?>
					<?php else: ?>
						Belum Dibeli
					<?php endif ?>
				</td>
				<td>
					<div class="btn-group" role="group" >
					  <a href="<?php echo base_url() ?>administration/editbarang/<?php echo $key->id_unit_barang ?>" class="btn btn-default btn-sm">Edit</a>
					  <a href="<?php echo base_url() ?>administration/hapusbarang/<?php echo $key->id_unit_barang ?>" class="btn btn-default btn-sm">Hapus</a>
					</div>
				</td>
			</tr>
		<?php endforeach ?>
	</table>
</div>
