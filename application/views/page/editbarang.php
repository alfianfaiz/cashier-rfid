<?php 
	$dataJenis = $this->Tool->getJenisList();
	$dataWarna = $this->Tool->getWarnaList();
	$dataBahan = $this->Tool->getBahanList();
 ?>
<div class="container-fluid">
	<h3>Form Edit Data Barang</h3>
	<dd>Anda dapat merubah data berikut ini</dd>
	<br>
	<?php foreach ($databarang as $key): ?>
		<form class="form-horizontal col-md-9" method="post" action="<?php echo base_url() ?>administration/actionsimpan/"  enctype="multipart/form-data">
			<input type="hidden" name="id_unit_barang" value="<?php echo $key->id_unit_barang ?>" required>
			<input type="hidden" name="nama_barang_old" value="<?php echo $key->nama_barang ?>" required>
			<input type="hidden" name="foto_barang" value="<?php echo $key->foto_barang ?>" required>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Nama Barang</label>
			    <div class="col-sm-9">
			      <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang" value="<?php echo $key->nama_barang ?>" required>
			    </div>
			</div>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Jenis Barang</label>
			    <div class="col-sm-9">
			      <select name="jenis_barang" class="form-control" required>
			      	<?php foreach ($dataJenis as $key2): ?>
			      		<option value="<?php echo underscore($key2->id_jenis) ?>" <?php if($key->jenis_id_jenis == $key2->id_jenis ) echo "selected"?>><?php echo $key2->nama_jenis ?></option>
			      	<?php endforeach ?>
			      </select>
			    </div>
			</div>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Bahan Barang</label>
			    <div class="col-sm-9">
			      <select name="bahan_barang" class="form-control" required>
			      	<?php foreach ($dataBahan as $key2): ?>
			      		<option value="<?php echo underscore($key2->id_bahan) ?>" <?php if($key->bahan_id_bahan == $key2->id_bahan ) echo "selected"?>><?php echo $key2->nama_bahan ?></option>
			      	<?php endforeach ?>
			      </select>
			    </div>
			</div>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Warna Barang</label>
			    <div class="col-sm-9">
			      <select name="warna_barang" class="form-control" required>
			      	<?php foreach ($dataWarna as $key2): ?>
			      		<option value="<?php echo underscore($key2->id_warna) ?>" <?php if($key->warna_id_warna == $key2->id_warna ) echo "selected"?>><?php echo $key2->nama_warna ?></option>
			      	<?php endforeach ?>
			      </select>
			    </div>
			</div>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Harga</label>
			    <div class="col-sm-9">
			      <input type="text" class="form-control" name="harga_barang" value="<?php echo $key->harga ?>" required>
			    </div>
			</div>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Kode Barang</label>
			    <div class=" col-sm-6">
			    	<input type="text" class="form-control" name="nomor_barang" value="<?php echo $key->nomor_unit ?>" required>
			    </div>
			</div>
			<div class="form-group">
			    <label class="col-sm-3 control-label">Gambar</label>
			    <div class=" col-sm-6">
			    	<input type="file" class="form-control" name="userfile">
			    </div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button class="btn btn-success form-control" type="submit">Simpan</button>
				</div>
			</div>
			
		</form>
	<?php endforeach ?>
</div>
