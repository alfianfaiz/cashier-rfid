-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Jul 2016 pada 05.31
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidan`
--

CREATE TABLE `bidan` (
  `id` int(11) NOT NULL,
  `kode_pendaftaran` varchar(50) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `puskesmas_id` int(11) NOT NULL,
  `nilai_akhir` float NOT NULL,
  `status_diterima` int(11) NOT NULL,
  `password` text NOT NULL,
  `image` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bidan`
--

INSERT INTO `bidan` (`id`, `kode_pendaftaran`, `nama`, `nik`, `tanggal_lahir`, `agama`, `alamat`, `puskesmas_id`, `nilai_akhir`, `status_diterima`, `password`, `image`) VALUES
(23, 'BY38J8IA', 'Faiz', '5302413039', '1231-02-13', 'Islam', 'ksjfkajkfa', 10, 6.91429, 0, 'bismillah', '20160622085553.jpg'),
(24, '2DPI7B3A', 'ajfaejfa', '530241', '0231-12-31', 'Budha', 'afjealkjeflkaj', 10, 0, 0, 'bismillah', '20160622090551.jpg'),
(25, 'U31P8CXD', 'Alfian Faiz', '53024130399', '2311-12-31', 'Islam', 'jakdfljakjdf', 10, 0, 0, 'bismillah', '20160623034410.jpg'),
(26, 'BT3N2AS1', 'nina', '12345', '2016-06-08', 'Islam', 'GAng Pete', 10, 0, 0, '12345', '20160623041828.jpg'),
(27, 'GSVR56K6', 'majid', '5302413062', '1996-04-04', 'Islam', 'kudus', 10, 6.67143, 1, 'xxxx', '20160623042509.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidan_kriteria_nilai`
--

CREATE TABLE `bidan_kriteria_nilai` (
  `id` int(11) NOT NULL,
  `bidan_id` int(11) NOT NULL,
  `kelas_kriteria_id` int(11) NOT NULL,
  `skor` float NOT NULL,
  `gap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bidan_kriteria_nilai`
--

INSERT INTO `bidan_kriteria_nilai` (`id`, `bidan_id`, `kelas_kriteria_id`, `skor`, `gap`) VALUES
(288, 24, 14, 4.5, 3),
(289, 24, 21, 3.5, 4),
(290, 24, 30, 7, 0),
(291, 24, 36, 7, 0),
(292, 24, 38, 7, 0),
(293, 24, 42, 7, 0),
(294, 24, 47, 7, 0),
(295, 24, 55, 7, 0),
(362, 23, 14, 7, 0),
(363, 23, 21, 7, 0),
(364, 23, 30, 7, 0),
(365, 23, 36, 7, 0),
(366, 23, 38, 7, 0),
(367, 23, 42, 7, 0),
(368, 23, 47, 7, 0),
(369, 23, 55, 7, 0),
(370, 23, 62, 6, -1),
(371, 23, 64, 7, 0),
(372, 23, 68, 7, 0),
(373, 25, 14, 3.5, 4),
(374, 25, 21, 3.5, 4),
(375, 25, 30, 6, 1),
(376, 25, 36, 7, 0),
(377, 25, 38, 7, 0),
(378, 25, 42, 7, 0),
(379, 25, 47, 7, 0),
(380, 25, 55, 7, 0),
(381, 26, 14, 3.5, 4),
(382, 26, 21, 4.5, 3),
(383, 26, 30, 6, 1),
(384, 26, 36, 6, 1),
(385, 26, 38, 4.5, 3),
(386, 26, 42, 6, 1),
(387, 26, 47, 4, -3),
(388, 26, 55, 6, 1),
(400, 27, 14, 7, 0),
(401, 27, 21, 7, 0),
(402, 27, 30, 7, 0),
(403, 27, 36, 7, 0),
(404, 27, 38, 7, 0),
(405, 27, 42, 7, 0),
(406, 27, 47, 7, 0),
(407, 27, 55, 7, 0),
(408, 27, 60, 6.5, 1),
(409, 27, 65, 6, -1),
(410, 27, 70, 5, -2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas_kriteria`
--

CREATE TABLE `kelas_kriteria` (
  `id` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `skor` int(11) NOT NULL,
  `kriteria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas_kriteria`
--

INSERT INTO `kelas_kriteria` (`id`, `nama_kelas`, `skor`, `kriteria_id`) VALUES
(10, '0 - 2 Km', 7, 1),
(11, '2,1 – 3 Km', 6, 1),
(12, '3,1 – 4 Km', 5, 1),
(13, '4,1 – 5 Km', 4, 1),
(14, '5,1 – 6 Km', 3, 1),
(15, '6,1 – 7 Km', 2, 1),
(16, '> 7,1 Km ', 1, 1),
(17, 'Dalam Desa', 7, 5),
(18, 'Desa berbatasan', 6, 5),
(19, 'Perbatasan Kecamatan Desa', 5, 5),
(20, 'Dalam Desa Pemukiman', 4, 5),
(21, 'Perbatasan Desa Pemukiman', 3, 5),
(22, 'Dalam Kecamatan', 2, 5),
(23, 'Luar Kecamatan', 1, 5),
(30, '< 22 Tahun', 1, 2),
(31, '22– 23 Tahun', 2, 2),
(32, '24– 25 Tahun ', 3, 2),
(33, '26– 27 Tahun', 4, 2),
(34, '28– 29 Tahun', 5, 2),
(35, '>20 Tahun', 6, 2),
(36, 'DIII Kebidanan', 1, 6),
(37, 'DIV Kebidanan', 2, 6),
(38, 'IPK <2.75', 1, 7),
(39, 'IPK 2.75 – 3.00 ', 2, 7),
(40, 'IPK 3.01 – 3.25', 3, 7),
(41, 'IPK > 3.25 ', 4, 7),
(42, 'Belum Kawin', 1, 8),
(43, 'Kawin ', 2, 8),
(44, 'Janda', 3, 8),
(45, 'Kawin', 2, 8),
(46, 'Janda', 3, 8),
(47, 'Rumah Sakit Umum', 5, 3),
(48, 'Puskemas', 4, 3),
(49, 'Klinik Bersalin', 3, 3),
(50, 'Magang / Kerja pada Bidan', 2, 3),
(51, 'Tidak Ada ', 1, 3),
(55, '0 – 3 Bulan ', 1, 9),
(56, '4 – 6 Bulan', 2, 9),
(57, '7 – 9 Bulan ', 3, 9),
(58, '10 – 12 Bulan', 4, 9),
(59, 'Lebih dari 1 tahun', 5, 9),
(60, 'Mahir', 4, 4),
(61, 'Mampu', 3, 4),
(62, 'Perlu Perbaikan', 2, 4),
(63, 'Buruk', 1, 4),
(64, 'Mahir', 4, 10),
(65, 'Mampu', 3, 10),
(66, 'Perlu Perbaikan', 2, 10),
(67, 'Buruk', 1, 10),
(68, 'Mahir', 4, 11),
(69, 'Mampu', 3, 11),
(70, 'Perlu Perbaikan', 2, 11),
(71, 'Buruk', 1, 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE `kontak` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(120) NOT NULL,
  `pesan` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id`, `nama`, `email`, `pesan`, `create_date`) VALUES
(1, 'Faiz', 'klfjakefa@gmail.com', 'kljlkjalkjfafgaf		      	\r\n		      ', '2016-06-22 17:39:32'),
(2, '', '', '		      	\r\n		      ', '2016-06-22 17:42:36'),
(3, 'Coba', 'jfakljef@gmail.com', 'klajfljaoeifafe		      	\r\n		      ', '2016-06-22 19:03:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int(11) NOT NULL,
  `kriteria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kriteria`
--

INSERT INTO `kriteria` (`id`, `kriteria`) VALUES
(1, 'Jarak 1'),
(2, 'Evaluasi Diri 1'),
(3, 'Pengalaman Kerja 1'),
(4, 'Uji Kompetensi 1'),
(5, 'Jarak 2'),
(6, 'Evaluasi Diri 2'),
(7, 'Evaluasi Diri 3'),
(8, 'Evaluasi Diri 4'),
(9, 'Pengalaman Kerja 2'),
(10, 'Uji Kompetensi 2'),
(11, 'uji Kompetensi 3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `puskesmas`
--

CREATE TABLE `puskesmas` (
  `id` int(11) NOT NULL,
  `kelurahan` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `puskesmas`
--

INSERT INTO `puskesmas` (`id`, `kelurahan`, `status`) VALUES
(10, 'Banaran', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `puskesmas_kriteria`
--

CREATE TABLE `puskesmas_kriteria` (
  `id` int(11) NOT NULL,
  `puskesmas_id` int(11) NOT NULL,
  `kelas_kriteria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `puskesmas_kriteria`
--

INSERT INTO `puskesmas_kriteria` (`id`, `puskesmas_id`, `kelas_kriteria_id`) VALUES
(103, 10, 14),
(104, 10, 21),
(105, 10, 30),
(106, 10, 36),
(107, 10, 38),
(108, 10, 42),
(109, 10, 47),
(110, 10, 55),
(111, 10, 61),
(112, 10, 64),
(113, 10, 68);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bidan`
--
ALTER TABLE `bidan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bidan_puskesmas` (`puskesmas_id`);

--
-- Indexes for table `bidan_kriteria_nilai`
--
ALTER TABLE `bidan_kriteria_nilai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bidan_kriteria_nilai_bidan` (`bidan_id`),
  ADD KEY `bidan_kriteria_nilai_kelas_kriteria` (`kelas_kriteria_id`);

--
-- Indexes for table `kelas_kriteria`
--
ALTER TABLE `kelas_kriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelas_kriteria_kriteria` (`kriteria_id`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puskesmas`
--
ALTER TABLE `puskesmas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puskesmas_kriteria`
--
ALTER TABLE `puskesmas_kriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `puskesmas_kriteria_nilai_kelas_kriteria` (`kelas_kriteria_id`),
  ADD KEY `puskesmas_kriteria_nilai_puskesmas` (`puskesmas_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bidan`
--
ALTER TABLE `bidan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `bidan_kriteria_nilai`
--
ALTER TABLE `bidan_kriteria_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;
--
-- AUTO_INCREMENT for table `kelas_kriteria`
--
ALTER TABLE `kelas_kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `puskesmas`
--
ALTER TABLE `puskesmas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `puskesmas_kriteria`
--
ALTER TABLE `puskesmas_kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `bidan`
--
ALTER TABLE `bidan`
  ADD CONSTRAINT `bidan_puskesmas` FOREIGN KEY (`puskesmas_id`) REFERENCES `puskesmas` (`id`);

--
-- Ketidakleluasaan untuk tabel `bidan_kriteria_nilai`
--
ALTER TABLE `bidan_kriteria_nilai`
  ADD CONSTRAINT `bidan_kriteria_nilai_bidan` FOREIGN KEY (`bidan_id`) REFERENCES `bidan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bidan_kriteria_nilai_kelas_kriteria` FOREIGN KEY (`kelas_kriteria_id`) REFERENCES `kelas_kriteria` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelas_kriteria`
--
ALTER TABLE `kelas_kriteria`
  ADD CONSTRAINT `kelas_kriteria_kriteria` FOREIGN KEY (`kriteria_id`) REFERENCES `kriteria` (`id`);

--
-- Ketidakleluasaan untuk tabel `puskesmas_kriteria`
--
ALTER TABLE `puskesmas_kriteria`
  ADD CONSTRAINT `puskesmas_kriteria_nilai_kelas_kriteria` FOREIGN KEY (`kelas_kriteria_id`) REFERENCES `kelas_kriteria` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `puskesmas_kriteria_nilai_puskesmas` FOREIGN KEY (`puskesmas_id`) REFERENCES `puskesmas` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
