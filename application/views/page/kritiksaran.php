<div class="container-fluid">
	<h3>Data Petugas</h3>
	<div class="col-sm-3 nopadding">
		<form class="form-horizontal">
		<div class="form-group">
		    <div class="col-sm-9">
		      <input type="date" name="tanggal" class="form-control">
		    </div>
		    <button class="btn btn-default">Cari</button>
		</div>
		</form>
	</div>

	<table class="table table-striped">
		<tr>
			<td>Nama</td>
			<td>Email</td>
			<td>Kritik / Saran</td>
			<td>Aksi</td>
		</tr>
		<tr>
			<td>Username</td>
			<td>Email</td>
			<td>Kritik / Saran</td>
			<td>
				<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">Balas</button>
			</td>
		</tr>
		<tr>
			<td>Username</td>
			<td>Email</td>
			<td>Kritik / Saran</td>
			<td>
				<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">Balas</button>
			</td>
		</tr>
	</table>
</div>

<!-- Modal -->
		<form>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <center><h4>Balas Kritik & Saran</h4></center>
		      </div>
		      <div class="modal-body">
		        <div class="container-fluid">
		        	
		        		<div class="form-group">
						    <label>Email Penerima</label>
							<input type="email" class="form-control" placeholder="Email">
						</div>
						<div class="form-group">
						    <label>Pesan balasan</label>
							<textarea class="form-control" placeholder="pesan ..">
								
							</textarea>
						</div>
		        	
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Kirim Balasan</button>
		      </div>
		    </div>
		  </div>
		</div>
		</form>