<div class="container-fluid">

	<?php 
	$cek = 0;
	foreach ($databarang as $key): $cek++;?>
		<h3>Anda Yakin ingin menghapus data ini ? </h3>

		<table class="table">
			<tr>
				<td>Nama</td>
				<td>Nomor Unit</td>
				<td>Dibeli</td>
			</tr>
			<tr>
				<td><?php echo $key->nama_barang ?></td>
				<td><?php echo $key->nomor_unit ?></td>
				<td><?php echo $key->pembelian_id_pembelian ? 'Dibeli' : 'Belum Dibeli' ?></td>
			</tr>
		</table>
		<form method="post">
			<input type="hidden" name="id_unit_barang" value="<?php echo $key->id_unit_barang ?>">
			<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Hapus </button>
		</form>
	<?php endforeach ?>
	<?php if ($cek == 0): ?>
		Data Tidak Ditemukan !
	<?php endif ?>
</div>