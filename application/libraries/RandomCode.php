<?php
	/**
	* Library for Generate Random Code
	*/
	class RandomCode
	{
		
		function __construct()
		{
			# code...
		}

		function RandomPass($uper,$numeric){
			$passOrder	= array();
			$password	= '';
			//=====================================
			//Generasi kombinasi dari alpha numeric
			//=====================================
			for ($i=0;$i<$uper;$i++){
				$passOrder[] = chr(rand(65,90));
			}
			//generasi numeric random
			for ($i=0;$i<$numeric;$i++){
				$passOrder[] = chr(rand(48,57));
			}
			shuffle($passOrder);
			
			foreach($passOrder as $char){
				$password .= $char;
			}
			return $password;	
		}


		// echo RandomPass();
	}
?>